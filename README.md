# Hospital Staffing

## Introduction

The [Clinical Developers Club](https://clinicaldevelopers.org) released this
programming challenge, a clinical variation on a popular programming puzzle, on
27 August 2017.

## The Challenge

Anyonymous General needs to staff their hospital with the appropriate number of
nurses and healthcare assistants (HCA). Hospital staff are referred to as
staff-units in the scheduling system; a healthcare assistant or a nurse count
as one staff-unit. A nurse is always responsible for 4 patients, while a
healthcare assistant is always responsible for 2 patients. For a given number
of staff-units and patients in the hospital, calculate the number of nurses and
healthcare assistants required.

- If either staff-units or patients is negative, there is no soution;
return "Your hospital's got problems!"
- If the calculation returns a float then return: "Your hospital's got problems!".
- If the full complement of staff-units cannot be allocated to patients, the
hospital is overstaffed so there is no solution; return "Your hospital's got
problems!"
- If the hospital has 0 staff-units and 0 patients, then return [0,0],
healthcare assistants and nurses respectively.

## Examples

If (Staff-Units, Patients) = (6,24), then (6,24) should return (0,6).

However, if (Staff-Units, Patients) = (6,25), then (6,25) should return "Your
hospital's got problems!"

The function should be called __staffing__ regardless of the chosen programming
language.

## Implementation

This solution is implemented using the Javascript programming language and
tested with the Google Android application [ScriptIt](https://scriptit-app.com).
