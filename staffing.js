///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Script Name: staffing.js                                                  //
// Creator: Gregory D. Horne (greg at gregoryhorne dot ca)                   //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////


var staff = prompt ('Enter staff:');
var patients = prompt ('Enter patients:');

// Given a number of staff-units and patients in the hospital, calculate the
// number of nurses and healthcare assistants there must be in the hospital
// to provide care to the patients. For every 4 patients admitted to hospital
// there is 1 nurse and 2 healthcare assistants.

function staffing(staff_units, patients)
{
    // Return true if the number is a floating-point number,
    // otherwise return false.

    function isFloat(n)
    {
        return Number(n) === n && n % 1 !== 0;
    }

    // each nurse is always responsible for 4 patients
    var nurse = Math.floor(patients / 4);
    
    // each healthcare assistant is always responsible for 2 patients
    // so number of healthcare assistants is twice that for nurses
    var hca = nurse * 2;
 
    // determine whether staffing is sufficient
    if (staff_units < 0 || patients < 0)
        return("Your hospital's got problems!");
    if (hca + nurse > staff_units || isFloat(patients / 4))
        return("Your hospital's got problems!");
    else
        return([hca, nurse]);
}


// Allocate staff to patients and report outcome.

var result = staffing(staff, patients);
if (typeof result === 'string')
    alert(result);
else
    alert('(' + result + ')');
